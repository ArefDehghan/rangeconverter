﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RefactoringPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            var rangeConverter = new RangeConverter();
            var ranges = rangeConverter.ConvertStringToRange("1-2, 4-6, 7-10");

            PrintRanges(ranges);
        }

        private static void PrintRanges(IEnumerable<Range> ranges)
        {
            foreach (var range in ranges)
                Console.WriteLine($"{range.Start} - {range.End}");
        }
    }
}
