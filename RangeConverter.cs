﻿using System;
using System.Collections.Generic;

namespace RefactoringPractice
{
    public class RangeConverter
    {
        public IEnumerable<Range> ConvertStringToRange(string input)
        {
            RemoveWhiteSpaces(input);
            var splitedInput = Split(input, ",");
            var ranges = GetRanges(splitedInput);

            return ranges;
        }

        private IEnumerable<Range> GetRanges(string[] inputs)
        {
            var ranges = new List<Range>();

            foreach (var i in inputs)
            {
                var range = Split(i, "-");
                var start = int.Parse(range[0]);
                var end = int.Parse(range[1]);

                ranges.Add(new Range(start, end));
            }

            return ranges;
        }

        private string RemoveWhiteSpaces(string input)
        {
            return input.Replace(" ", "");
        }

        private string[] Split(string input, string separator)
        {
            return input.Split(separator);
        }
    }
}
